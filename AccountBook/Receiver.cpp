#include "Receiver.h"
#include <QLocalSocket>

Receiver::Receiver()
    : m_sizeReceivedData(0)
{
}

void Receiver::readyRead()
{
    QLocalSocket* socket = dynamic_cast<QLocalSocket*>(sender());

    while (socket->bytesAvailable() > 0) {
        if (m_sizeReceivedData == 0) {
            if (socket->bytesAvailable() >= static_cast<qint64>(sizeof(m_sizeReceivedData))) {
                QByteArray data = socket->read(sizeof(m_sizeReceivedData));
                QDataStream stream(data);
                stream >> m_sizeReceivedData;
            } else {
                break;
            }
        } else {
            if (socket->bytesAvailable() >= m_sizeReceivedData) {
                Command command = Command::deserialize(socket->read(m_sizeReceivedData));
                m_sizeReceivedData = 0;
                emit newCommand(command);
            } else {
                break;
            }
        }
    }
}
