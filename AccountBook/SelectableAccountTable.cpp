#include "SelectableAccountTable.h"

SelectableAccountTable::SelectableAccountTable()
    : m_selectedRow(-1)
    , m_selectedColumn(-1)
{
}

void SelectableAccountTable::setSelectedRow(int row)
{
    m_selectedRow = row;
}

int SelectableAccountTable::selectedRow() const
{
    return m_selectedRow;
}

bool SelectableAccountTable::rowIsSelected(int row) const
{
    return ((m_selectedRow != -1) && (m_selectedRow == row));
}

void SelectableAccountTable::resetSelectedRow()
{
    m_selectedRow = -1;
}

void SelectableAccountTable::setSelectedColumn(int column)
{
    m_selectedColumn = column;
}

int SelectableAccountTable::selectedColumn() const
{
    return m_selectedColumn;
}

bool SelectableAccountTable::columnIsSelected(int column) const
{
    return ((m_selectedColumn != -1) && (m_selectedColumn == column));
}

void SelectableAccountTable::resetSelectedColumn()
{
    m_selectedColumn = -1;
}
