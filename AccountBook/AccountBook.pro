#-------------------------------------------------
#
# Project created by QtCreator 2017-08-05T16:29:03
#
#-------------------------------------------------

QT  += core gui
QT  += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AccountBook
TEMPLATE = app


SOURCES  += main.cpp\
    MainWindow.cpp \
    SharedDataService.cpp \
    SharedInstanceCounter.cpp \
    ./Delegates/RadioButtonCenterAligningDelegate.cpp \
    Account.cpp \
    Command.cpp \
    Sender.cpp \
    Receiver.cpp \
    ./Delegates/PixmapAligningDelegate.cpp \
    PixmapProvider.cpp \
    ./Delegates/TextPixmapAligningDelegate.cpp \
    AccountTable.cpp \
    AccountTableModel.cpp \
    DiskStorageAccountTable.cpp \
    SelectableAccountTable.cpp \
    Delegates/TextDelegate.cpp

HEADERS  += MainWindow.h \
    SharedDataService.h \
    SharedInstanceCounter.h \
    ./Delegates/RadioButtonCenterAligningDelegate.h \
    Account.h \
    Command.h \
    Sender.h \
    Receiver.h \
    ./Delegates/PixmapAligningDelegate.h \
    PixmapProvider.h \
    ./Delegates/TextPixmapAligningDelegate.h \
    AccountTable.h \
    AccountTableModel.h \
    DiskStorageAccountTable.h \
    SelectableAccountTable.h \
    Delegates/TextDelegate.h

FORMS    += MainWindow.ui

CONFIG   += c++11

INCLUDEPATH += Delegates

RESOURCES += res.qrc
