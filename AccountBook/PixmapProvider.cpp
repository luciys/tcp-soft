#include "PixmapProvider.h"

const QPixmap& PixmapProvider::appPixmap()
{
    static QPixmap appPixmap(":/res/Icons/account-128.png");

    return appPixmap;
}

const QPixmap& PixmapProvider::phonePixmap()
{
    static QPixmap phonePixmap(":/res/Icons/phone-512.png");

    return phonePixmap;
}

const QPixmap& PixmapProvider::removePixmap()
{
    static QPixmap removePixmap(":/res/Icons/remove-512.png");

    return removePixmap;
}

const QPixmap& PixmapProvider::addPixmap()
{
    static QPixmap addPixmap(":/res/Icons/add-512.png");

    return addPixmap;
}
