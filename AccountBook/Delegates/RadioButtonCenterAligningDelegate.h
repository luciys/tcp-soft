#ifndef DECORATIONALIGNINGDELEGATE_H
#define DECORATIONALIGNINGDELEGATE_H

#include <QItemDelegate>
#include <QStyleOptionViewItem>

class RadioButtonCenterAligningDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit RadioButtonCenterAligningDelegate(QObject* parent = nullptr);
    virtual void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};

#endif // DECORATIONALIGNINGDELEGATE_H
