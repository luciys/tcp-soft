#ifndef TEXTPIXMAPALIGNINGDELEGATE_H
#define TEXTPIXMAPALIGNINGDELEGATE_H

#include "PixmapAligningDelegate.h"
#include "TextDelegate.h"

class TextPixmapAligningDelegate : public QItemDelegate
{
public:
    explicit TextPixmapAligningDelegate(Qt::AlignmentFlag pixmapAlignment, QObject* parent = nullptr);
    explicit TextPixmapAligningDelegate(int maxTextLength, Qt::AlignmentFlag pixmapAlignment, QObject* parent = nullptr);

    virtual void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
    virtual void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;

private:
    TextDelegate m_textDelegate;
    PixmapAligningDelegate m_pixmapAligningDelegate;
};

#endif // TEXTPIXMAPALIGNINGDELEGATE_H
