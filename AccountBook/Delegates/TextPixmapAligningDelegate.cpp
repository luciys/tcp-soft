#include "TextPixmapAligningDelegate.h"
#include <QFontMetrics>
#include <QPainter>

TextPixmapAligningDelegate::TextPixmapAligningDelegate(Qt::AlignmentFlag pixmapAlignment, QObject* parent)
    : TextPixmapAligningDelegate(-1, pixmapAlignment, parent)
{
}

TextPixmapAligningDelegate::TextPixmapAligningDelegate(int maxTextLength, Qt::AlignmentFlag pixmapAlignment, QObject* parent)
    : QItemDelegate(parent)
    , m_textDelegate(maxTextLength)
    , m_pixmapAligningDelegate(pixmapAlignment)
{
}

void TextPixmapAligningDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    m_textDelegate.updateEditorGeometry(editor, option, index);
}

void TextPixmapAligningDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    m_textDelegate.paint(painter, option, index);
    m_pixmapAligningDelegate.paint(painter, option, index);
}
