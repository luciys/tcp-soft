#ifndef PIXMAPALIGNINGDELEGATE_H
#define PIXMAPALIGNINGDELEGATE_H

#include <QItemDelegate>

class PixmapAligningDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit PixmapAligningDelegate(Qt::AlignmentFlag alignment, QObject* parent = nullptr);

    virtual void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;

private:
    Qt::AlignmentFlag m_alignment;
};

#endif // PIXMAPALIGNINGDELEGATE_H
