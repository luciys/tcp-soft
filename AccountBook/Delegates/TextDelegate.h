#ifndef TEXTDELEGATE_H
#define TEXTDELEGATE_H

#include <QItemDelegate>

class TextDelegate : public QItemDelegate
{
public:
    explicit TextDelegate(QObject* parent = nullptr);
    explicit TextDelegate(int maxLength, QObject* parent = nullptr);

    virtual void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
    virtual void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const override;

    void setMaxLength(int maxLength);

private:
    int m_maxLength;
};

#endif // TEXTDELEGATE_H
