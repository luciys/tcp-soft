#include "RadioButtonCenterAligningDelegate.h"
#include <QApplication>

RadioButtonCenterAligningDelegate::RadioButtonCenterAligningDelegate(QObject* parent)
    : QItemDelegate(parent)
{
}

void RadioButtonCenterAligningDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QStyleOptionViewItem newOption = option;

    if (index.data(Qt::CheckStateRole) == Qt::Checked) {
        newOption.state |= QStyle::State_On;
        newOption.state &= ~QStyle::State_Off;
    } else {
        newOption.state |= QStyle::State_Off;
        newOption.state &= ~QStyle::State_On;
    }

    drawBackground(painter, newOption, index);
    QApplication::style()->drawPrimitive(QStyle::PE_IndicatorRadioButton, &newOption, painter);
}
