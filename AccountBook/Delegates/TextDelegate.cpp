#include "TextDelegate.h"
#include <QLineEdit>
#include <QFontMetrics>
#include <QPainter>

TextDelegate::TextDelegate(QObject* parent)
    : TextDelegate(-1, parent)
{
}

TextDelegate::TextDelegate(int maxLength, QObject* parent)
    : QItemDelegate(parent)
    , m_maxLength(maxLength)
{
}

void TextDelegate::setMaxLength(int maxLength)
{
    m_maxLength = maxLength;
}

void TextDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(index);
    editor->setGeometry(option.rect);
    editor->setFixedWidth(option.rect.width());

    if (m_maxLength != -1) {
        QLineEdit* lineEdit = dynamic_cast<QLineEdit*>(editor);
        if (lineEdit)
            lineEdit->setMaxLength(m_maxLength);
    }
}

void TextDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QFontMetrics metrics(painter->font());
    QRect rectText = option.rect;
    rectText.setLeft(rectText.left() + metrics.strikeOutPos());
    rectText.setTop(rectText.top() + (rectText.height() - metrics.height()) / 2);
    painter->drawText(rectText, index.data().toString());
}
