#include "PixmapAligningDelegate.h"

namespace {

const int kMaxIconSize = 32;
const double kPixmapFactorRelativeCellSize = 0.7;

} // namespace

PixmapAligningDelegate::PixmapAligningDelegate(Qt::AlignmentFlag alignment, QObject* parent)
    : QItemDelegate(parent)
    , m_alignment(alignment)
{
}

void PixmapAligningDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QPixmap pixmap = index.data(Qt::DecorationRole).value<QPixmap>();
    if (!pixmap.isNull()) {
        int size = qMin(option.rect.height(), option.rect.width());
        int factorizableSize = static_cast<int>(kPixmapFactorRelativeCellSize * size);
        if (factorizableSize > kMaxIconSize)
            factorizableSize = kMaxIconSize;

        QRect pixmapRect = option.rect;
        if (m_alignment == Qt::AlignLeft)
            pixmapRect.setLeft(pixmapRect.left() - pixmapRect.width() / 2 - factorizableSize);
        else if (m_alignment == Qt::AlignRight)
            pixmapRect.setLeft(pixmapRect.left() + pixmapRect.width() - factorizableSize);

        drawBackground(painter, option, index);
        drawDecoration(painter, option, pixmapRect, pixmap.scaled(factorizableSize, factorizableSize, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    } else {
        QItemDelegate::paint(painter, option, index);
    }
}
