#include "AccountTableModel.h"
#include "PixmapProvider.h"

int AccountTableModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return m_selectableAccountTable.size();
}

int AccountTableModel::columnCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return static_cast<int>(ColumnType::COLUMN_COUNT);
}

QVariant AccountTableModel::data(const QModelIndex& index, int role) const
{
    int row = index.row();
    if (!index.isValid() || !rowIsValid(row))
        return QVariant();

    ColumnType columnType = static_cast<ColumnType>(index.column());
    switch (role) {
    case Qt::BackgroundRole:
    {
        if (m_selectableAccountTable.rowIsSelected(row))
            return selectedRowBackground();
        return unselectedRowBackground();
    }
    case Qt::CheckStateRole:
    {
        if (columnType == ColumnType::SELECTED) {
            if (m_selectableAccountTable.rowIsSelected(row))
                return Qt::Checked;
            return Qt::Unchecked;
        }
        return QVariant();
    }
    case Qt::DecorationRole:
    {
        if (m_selectableAccountTable.rowIsSelected(row)) {
            if (columnType == ColumnType::REMOVE_BUTTON)
                return PixmapProvider::removePixmap();
            if (columnType == ColumnType::ACCOUNT_NAME)
                return PixmapProvider::phonePixmap();
        }
        return QVariant();
    }
    case Qt::DisplayRole:
    case Qt::EditRole:
    {
        const Account& account = m_selectableAccountTable.account(row);
        switch (columnType) {
        case ColumnType::ACCOUNT_NAME:
            return account.name();
        case ColumnType::ACCOUNT_TYPE:
            return account.type();
        case ColumnType::ACCOUNT_COMMENT:
            return account.comment();
        default:
            return QVariant();
        }
    }
    default:
    {
        return QVariant();
    }
    }
}

bool AccountTableModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    int row = index.row();
    if ((role != Qt::EditRole)
        || !index.isValid()
        || !rowIsValid(row))
        return false;

    ColumnType columnType = static_cast<ColumnType>(index.column());
    Command command;
    QString newValue;
    switch (columnType) {
    case ColumnType::ACCOUNT_NAME:
        newValue = value.toString();
        if (m_selectableAccountTable.account(row).name() != newValue) {
            command = Command::buildUpdateNameCommand(row, newValue);
            command.executeWith(&m_selectableAccountTable, this);
            emit newCommand(command);
        }
        break;
    case ColumnType::ACCOUNT_TYPE:
        newValue = value.toString();
        if (m_selectableAccountTable.account(row).type() != newValue) {
            command = Command::buildUpdateTypeCommand(row, newValue);
            command.executeWith(&m_selectableAccountTable, this);
            emit newCommand(command);
        }
        break;
    case ColumnType::ACCOUNT_COMMENT:
        newValue = value.toString();
        if (m_selectableAccountTable.account(row).comment() != newValue) {
            command = Command::buildUpdateCommentCommand(row, newValue);
            command.executeWith(&m_selectableAccountTable, this);
            emit newCommand(command);
        }
        break;
    default:
        break;
    }
    emit dataChanged(index, index);

    return true;
}

QVariant AccountTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Vertical)
        return section;

    ColumnType columnType = static_cast<ColumnType>(section);
    switch (columnType) {
    case ColumnType::ACCOUNT_NAME:
        return trUtf8("Телефон");
    case ColumnType::ACCOUNT_TYPE:
        return trUtf8("Тип");
    case ColumnType::ACCOUNT_COMMENT:
        return trUtf8("Комментарий");
    case ColumnType::SELECTED:
    case ColumnType::REMOVE_BUTTON:
    default:
        return QVariant();
    }
}

Qt::ItemFlags AccountTableModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return QAbstractItemModel::flags(index);

    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    ColumnType columnType = static_cast<ColumnType>(index.column());
    switch (columnType) {
    case ColumnType::ACCOUNT_NAME:
    case ColumnType::ACCOUNT_TYPE:
    case ColumnType::ACCOUNT_COMMENT:
        flags |= Qt::ItemIsEditable;
        break;
    case ColumnType::SELECTED:
    case ColumnType::REMOVE_BUTTON:
        flags |= Qt::ItemIsEnabled;
        break;
    default:
        break;
    }

    return flags;
}

void AccountTableModel::updateCell(int row, int column)
{
    if (rowIsValid(row)) {
        ColumnType columnType = static_cast<ColumnType>(column);
        switch (columnType) {
        case ColumnType::ACCOUNT_NAME:
        case ColumnType::ACCOUNT_TYPE:
        case ColumnType::ACCOUNT_COMMENT:
            emit layoutChanged();
            emit dataChanged(index(row, column), index(row, column));
            break;
        default:
            break;
        }
    }
}

void AccountTableModel::beginRemoveOneRow(int row)
{
    QAbstractItemModel::beginRemoveRows(m_emptyIndex, row, row);
}

void AccountTableModel::endRemoveOneRow()
{
    QAbstractItemModel::endRemoveRows();
}

void AccountTableModel::beginInsertOneRow(int row)
{
    QAbstractItemModel::beginInsertRows(m_emptyIndex, row, row);
}

void AccountTableModel::endInsertOneRow()
{
    QAbstractItemModel::endInsertRows();
}

void AccountTableModel::processCommand(const Command& command)
{
    command.executeWith(&m_selectableAccountTable, this);
}

void AccountTableModel::updateStateRow(const QModelIndex& index)
{
    int row = index.row();
    if (!index.isValid() || !rowIsValid(row))
        return;

    ColumnType columnType = static_cast<ColumnType>(index.column());
    if ((columnType == ColumnType::REMOVE_BUTTON)
        && m_selectableAccountTable.rowIsSelected(row)) {
        m_selectableAccountTable.resetSelectedRow();
        Command command = Command::buildRemoveCommand(row);
        emit newCommand(command);
    } else {
        m_selectableAccountTable.setSelectedRow(row);
        emit dataChanged(m_emptyIndex, m_emptyIndex);
    }
}

void AccountTableModel::setSelectedCell(const QModelIndex& index)
{
    if (!index.isValid() || !rowIsValid(index.row()))
        return;
    m_selectableAccountTable.setSelectedColumn(index.column());
}

void AccountTableModel::resetSelectedCell(QWidget* editor, QAbstractItemDelegate::EndEditHint hint)
{
    Q_UNUSED(editor);
    Q_UNUSED(hint);
    m_selectableAccountTable.resetSelectedColumn();
}

QColor AccountTableModel::selectedRowBackground() const
{
    static QColor transparentYellow(255, 255, 0, 40);
    return transparentYellow;
}

QColor AccountTableModel::unselectedRowBackground() const
{
    return Qt::white;
}

bool AccountTableModel::rowIsValid(int row) const
{
    return ((row >= 0) && (row < m_selectableAccountTable.size()));
}
