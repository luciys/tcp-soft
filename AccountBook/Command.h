#ifndef COMMAND_H
#define COMMAND_H

#include "SelectableAccountTable.h"
#include <QString>
#include <QSharedPointer>

class AccountTableModel;

class Command
{
public:
    Command() = default;

    static Command buildAppendCommand(const QString& name, const QString& type, const QString& comment);
    static Command buildRemoveCommand(int index);
    static Command buildUpdateNameCommand(int index, const QString& name);
    static Command buildUpdateTypeCommand(int index, const QString& type);
    static Command buildUpdateCommentCommand(int index, const QString& comment);

    static Command deserialize(const QByteArray& bytes);

    virtual QByteArray serialize() const;
    virtual void executeWith(AccountTable* accountTable) const;
    virtual void executeWith(AccountTable* accountTable, AccountTableModel* accountTableModel) const;

protected:
    static void writeUtf(QDataStream& ds, const QString& param);
    static QString readUtf(QDataStream& stream);

protected:
    enum class Id : quint8 {
        UNKNOWN = 0,
        ADD,
        REMOVE,
        UPDATE_NAME,
        UPDATE_TYPE,
        UPDATE_COMMENT
    };

    Id m_id;

private:
    Command(Id commandId, QSharedPointer<Command> command);

private:
    QSharedPointer<Command> m_command;
};

class AppendCommand : public Command
{
public:
    AppendCommand(const QString& name, const QString& type, const QString& comment);
    AppendCommand(const AppendCommand&) = delete;

    virtual QByteArray serialize() const override;
    virtual void executeWith(AccountTable* accountTable, AccountTableModel* accountTableModel) const override;

private:
    QString m_name;
    QString m_type;
    QString m_comment;
};

class RemoveCommand : public Command
{
public:
    RemoveCommand(int index);
    RemoveCommand(const RemoveCommand&) = delete;

    virtual QByteArray serialize() const override;
    virtual void executeWith(AccountTable* accountTable, AccountTableModel* accountTableModel) const override;

private:
    int m_index;
};

class UpdateCommand : public Command
{
public:
    UpdateCommand(int index, const QString& value);
    UpdateCommand(const UpdateCommand&) = delete;

    virtual QByteArray serialize() const override;
    virtual void executeWith(AccountTable* accountTable, AccountTableModel* accountTableModel) const override;

private:
    int m_index;
    QString m_value;
};

#endif // COMMAND_H
