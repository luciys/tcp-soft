#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "RadioButtonCenterAligningDelegate.h"
#include "PixmapAligningDelegate.h"
#include "TextPixmapAligningDelegate.h"
#include "PixmapProvider.h"
#include "TextDelegate.h"

MainWindow::MainWindow(const QString& serviceName, QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(&m_model, &AccountTableModel::newCommand, &m_sender, &Sender::sendCommandToAll);
    connect(&m_socket, &QLocalSocket::readyRead, &m_receiver, &Receiver::readyRead);
    connect(&m_receiver, &Receiver::newCommand, &m_model, &AccountTableModel::processCommand);
    initAccountTable();
    connectToService(serviceName);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    m_socket.disconnectFromServer();
    m_socket.waitForDisconnected();
    event->accept();
    emit closed();
}

void MainWindow::on_appendAccountButton_clicked()
{
    m_sender.sendCommandToAll(Command::buildAppendCommand("", "", ""));
}

void MainWindow::initAccountTable()
{
    ui->accountsView->setModel(&m_model);
    ui->accountsView->setItemDelegateForColumn(0, new RadioButtonCenterAligningDelegate());
    ui->accountsView->setItemDelegateForColumn(1, new TextPixmapAligningDelegate(20, Qt::AlignRight));
    ui->accountsView->setItemDelegateForColumn(2, new TextDelegate(10));
    ui->accountsView->setItemDelegateForColumn(3, new TextDelegate(30));
    ui->accountsView->setItemDelegateForColumn(4, new PixmapAligningDelegate(Qt::AlignCenter));
    ui->accountsView->setFocusPolicy(Qt::NoFocus);
    ui->accountsView->setSelectionMode(QAbstractItemView::NoSelection);
    connect(ui->accountsView, &QAbstractItemView::clicked, &m_model, &AccountTableModel::updateStateRow);
    connect(ui->accountsView, &QAbstractItemView::doubleClicked, &m_model, &AccountTableModel::setSelectedCell);
    connect(ui->accountsView->itemDelegate(), &QAbstractItemDelegate::closeEditor, &m_model, &AccountTableModel::resetSelectedCell);
}

void MainWindow::connectToService(const QString& serverName)
{
    m_socket.connectToServer(serverName);
    m_socket.waitForConnected();
    if (m_socket.state() == QLocalSocket::ConnectedState)
        m_sender.addSocket(&m_socket);
}
