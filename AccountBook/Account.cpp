#include "Account.h"

Account::Account(const QString& name, const QString& type, const QString& comment)
    : m_name(name)
    , m_type(type)
    , m_comment(comment)
{
}

const QString& Account::name() const
{
    return m_name;
}

const QString& Account::type() const
{
    return m_type;
}

const QString& Account::comment() const
{
    return m_comment;
}

void Account::setName(const QString& name)
{
    m_name = name;
}

void Account::setType(const QString& type)
{
    m_type = type;
}

void Account::setComment(const QString& comment)
{
    m_comment = comment;
}
