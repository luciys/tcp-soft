#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "AccountTableModel.h"
#include "Receiver.h"
#include "Sender.h"
#include <QMainWindow>
#include <QCloseEvent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(const QString& serverName, QWidget* parent = nullptr);
    ~MainWindow();

Q_SIGNALS:
    void closed();

protected:
    virtual void closeEvent(QCloseEvent* event) override;

private Q_SLOTS:
    void on_appendAccountButton_clicked();

private:
    void initAccountTable();
    void connectToService(const QString& serviceName);

private:
    Ui::MainWindow *ui;
    AccountTableModel m_model;
    Sender m_sender;
    Receiver m_receiver;
    QLocalSocket m_socket;
};

#endif // MAINWINDOW_H
