#include "DiskStorageAccountTable.h"
#include <QList>
#include <QFile>

AccountTable DiskStorageAccountTable::fromFile(const QString& fileName)
{
    AccountTable accountTable;
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly)) {
        QDataStream stream(&file);
        QList<Account> accounts;
        stream >> accounts;
        accountTable.setAccounts(accounts);
    }

    return accountTable;
}

void DiskStorageAccountTable::toFile(const QString& fileName, const AccountTable& accountTable)
{
    QFile file(fileName);
    if (file.open(QIODevice::WriteOnly)) {
        QDataStream stream(&file);
        stream << accountTable.accounts();
    }
}

QDataStream& operator<<(QDataStream& stream, const Account& account)
{
    stream << account.name() << account.type() << account.comment();
    return stream;
}

QDataStream& operator>>(QDataStream& stream, Account& account)
{
    QString name;
    QString type;
    QString comment;
    stream >> name >> type >> comment;
    account.setName(name);
    account.setType(type);
    account.setComment(comment);
    return stream;
}
