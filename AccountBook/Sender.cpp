#include "Sender.h"

Sender::Sender()
{
}

void Sender::addSocket(QLocalSocket* socket)
{
    m_sockets.insert(socket);
}

void Sender::removeSocket(QLocalSocket* socket)
{
    m_sockets.remove(socket);
}

int Sender::countOfSockets() const
{
    return m_sockets.size();
}

void Sender::sendCommandToSocket(QLocalSocket* socket, const Command& command)
{
    send(socket, command.serialize());
}

void Sender::sendCommandToAll(const Command& command)
{
    if (!m_sockets.isEmpty()) {
        QByteArray data = command.serialize();
        foreach (QLocalSocket* socket, m_sockets)
            send(socket, data);
    }
}

void Sender::send(QLocalSocket* socket, const QByteArray& data)
{
    QByteArray sendingBytes;
    QDataStream stream(&sendingBytes, QIODevice::WriteOnly);
    stream << static_cast<qint64>(data.size());
    stream.writeRawData(data, data.size());

    socket->write(sendingBytes);
    socket->waitForBytesWritten();
}
