#ifndef RECEIVER_H
#define RECEIVER_H

#include "Command.h"

class Receiver : public QObject
{
    Q_OBJECT
public:
    Receiver();

Q_SIGNALS:
    void newCommand(const Command& command) const;

public Q_SLOTS:
    void readyRead();

private:
    qint64 m_sizeReceivedData;
};

#endif // RECEIVER_H
