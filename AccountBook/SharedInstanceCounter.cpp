#include "SharedInstanceCounter.h"

namespace {

const QString kSemaphoreName = "InstanceCounter::semaphore";
const QString kSharedMemoryName = "InstanceCounter::sharedMemory";

} // namespace

SharedInstanceCounter::SharedInstanceCounter()
    : m_semaphore(kSemaphoreName, 1)
    , m_sharedCounter(kSharedMemoryName)
    , m_isLocked(false)
{
}

void SharedInstanceCounter::lock()
{
    m_semaphore.acquire();
    m_isLocked = true;
}

void SharedInstanceCounter::unlock()
{
    m_semaphore.release();
    m_isLocked = false;
}

qint8 SharedInstanceCounter::count()
{
    return changeValue(0);
}

void SharedInstanceCounter::increment()
{
    changeValue(1);
}

void SharedInstanceCounter::decrement()
{
    changeValue(-1);
}

qint8 SharedInstanceCounter::changeValue(qint8 value)
{
    if (!m_isLocked)
        m_semaphore.acquire();

#if defined(Q_OS_LINUX) || defined(Q_OS_UNIX)
    QSharedMemory unixSharedMemory(kSharedMemoryName);
    if (unixSharedMemory.attach())
        unixSharedMemory.detach();
#endif

    if (!m_sharedCounter.attach())
        m_sharedCounter.create(sizeof(CountType));

    int countOfInstanceApp = 0;
    if (m_sharedCounter.lock()) {
        CountType* counter = static_cast<CountType*>(m_sharedCounter.data());
        if (counter) {
            (*counter) += value;
            countOfInstanceApp = *counter;
        }
        m_sharedCounter.unlock();
    }

    if (!m_isLocked)
        m_semaphore.release();

    return countOfInstanceApp;
}
