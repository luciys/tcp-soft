#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QString>

class Account
{
public:
    Account() = default;
    Account(const QString& name, const QString& type, const QString& comment);

    const QString& name() const;
    const QString& type() const;
    const QString& comment() const;

    void setName(const QString& name);
    void setType(const QString& type);
    void setComment(const QString& comment);

private:
    QString m_name;
    QString m_type;
    QString m_comment;
};

#endif // ACCOUNT_H
