#ifndef SHAREDINSTANCECOUNTER_H
#define SHAREDINSTANCECOUNTER_H

#include <QSystemSemaphore>
#include <QSharedMemory>

class SharedInstanceCounter
{
public:
    SharedInstanceCounter();

    void lock();
    void unlock();
    qint8 count();
    void increment();
    void decrement();

private:
    qint8 changeValue(qint8 value);

private:
    using CountType = qint8;
    QSystemSemaphore m_semaphore;
    QSharedMemory m_sharedCounter;
    bool m_isLocked;
};

#endif // SHAREDINSTANCECOUNTER_H
