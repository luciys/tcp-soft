#ifndef SHAREDDATASERVICE_H
#define SHAREDDATASERVICE_H

#include "SharedInstanceCounter.h"
#include "Account.h"
#include "AccountTable.h"
#include "Sender.h"
#include "Receiver.h"
#include <QLocalServer>
#include <QLocalSocket>
#include <QSharedPointer>

class SharedDataService : public QObject
{
    Q_OBJECT
public:
    SharedDataService(const QString& serverName, const QString& storageFileName);
    SharedDataService(const SharedDataService&) = delete;

public Q_SLOTS:
    void run();

Q_SIGNALS:
    void allConnectionsClosed();
    void closed();

private Q_SLOTS:
    void newConnection();
    void socketDisconnected();
    void processCommand(const Command& command);
    void sendCommandToAll(const Command& command);

private:
    void addNewSocket(QLocalSocket* socket);
    void sendAccounts(QLocalSocket* socket);

private:
    QString m_serverName;
    QString m_fileName;
    QSharedPointer<QLocalServer> m_server;
    Sender m_sender;
    QHash<QLocalSocket*, QSharedPointer<Receiver>> m_receivers;
    AccountTable m_accountTable;
};

#endif // SHAREDDATASERVICE_H
