#include "AccountTable.h"
#include <limits>

namespace {

const int kIntMax = std::numeric_limits<int>::max();

} // namespace


AccountTable::AccountTable()
    : m_size(0)
{
}

AccountTable::AccountTable(AccountTable& accountTable)
    : QObject()
    , m_accounts(accountTable.m_accounts)
    , m_size(accountTable.m_size)
{
}

bool AccountTable::append(const QString& name, const QString& type, const QString& comment)
{
    if (m_size == kIntMax)
        return false;

    m_accounts.append(Account(name, type, comment));
    ++m_size;

    return true;
}

void AccountTable::remove(int row)
{
    if (indexIsValid(row)) {
        m_accounts.removeAt(row);
        --m_size;
    }
}

void AccountTable::updateName(int row, const QString& name)
{
    if (indexIsValid(row))
        m_accounts[row].setName(name);
}

void AccountTable::updateType(int row, const QString& type)
{
    if (indexIsValid(row))
        m_accounts[row].setType(type);
}

void AccountTable::updateComment(int row, const QString& comment)
{
    if (indexIsValid(row))
        m_accounts[row].setComment(comment);
}

int AccountTable::size() const
{
    return m_size;
}

void AccountTable::setAccounts(const QList<Account>& accounts)
{
    m_accounts = accounts;
    m_size = m_accounts.size();
}

const QList<Account>& AccountTable::accounts() const
{
    return m_accounts;
}

const Account& AccountTable::account(int index) const
{
    if (indexIsValid(index))
        return m_accounts[index];

    static const Account emptyAccount;
    return emptyAccount;
}

AccountTable& AccountTable::operator=(AccountTable& accountTable)
{
    if (this == &accountTable)
        return *this;

    setAccounts(accountTable.accounts());
    return *this;
}

bool AccountTable::indexIsValid(int index) const
{
    return ((index >= 0) && (index < m_size));
}
