#ifndef ACCOUNTTABLEMODEL_H
#define ACCOUNTTABLEMODEL_H

#include "Command.h"
#include "SelectableAccountTable.h"
#include <QAbstractTableModel>
#include <QPixmap>
#include <QAbstractItemDelegate>

class AccountTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    AccountTableModel() = default;

    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex& index, int role) const override;
    virtual bool setData(const QModelIndex& index, const QVariant& value, int role) override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    virtual Qt::ItemFlags flags(const QModelIndex& index) const override;

    void updateCell(int row, int column);

    void beginRemoveOneRow(int row);
    void endRemoveOneRow();

    void beginInsertOneRow(int row);
    void endInsertOneRow();

Q_SIGNALS:
    void newCommand(const Command& command);

public Q_SLOTS:
    void processCommand(const Command& command);
    void updateStateRow(const QModelIndex& index);
    void setSelectedCell(const QModelIndex& index);
    void resetSelectedCell(QWidget* editor, QAbstractItemDelegate::EndEditHint hint);

private:
    enum class ColumnType;
    QColor selectedRowBackground() const;
    QColor unselectedRowBackground() const;
    bool rowIsValid(int row) const;

private:
    enum class ColumnType {
        SELECTED = 0,
        ACCOUNT_NAME,
        ACCOUNT_TYPE,
        ACCOUNT_COMMENT,
        REMOVE_BUTTON,
        COLUMN_COUNT
    };

    QModelIndex m_emptyIndex;
    SelectableAccountTable m_selectableAccountTable;
};

#endif // TABLEMODEL_H
