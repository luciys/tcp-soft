#include "Command.h"
#include "AccountTableModel.h"
#include <QDataStream>

Command Command::buildAppendCommand(const QString& name, const QString& type, const QString& comment)
{
    return Command(Id::ADD, QSharedPointer<AppendCommand>::create(name, type, comment));
}

Command Command::buildRemoveCommand(int index)
{
    return Command(Id::REMOVE, QSharedPointer<RemoveCommand>::create(index));
}

Command Command::buildUpdateNameCommand(int index, const QString& name)
{
    QSharedPointer<UpdateCommand> ptr = QSharedPointer<UpdateCommand>::create(index, name);
    ptr->m_id = Id::UPDATE_NAME;
    return Command(Id::UPDATE_NAME, ptr);
}

Command Command::buildUpdateTypeCommand(int index, const QString& type)
{
    QSharedPointer<UpdateCommand> ptr = QSharedPointer<UpdateCommand>::create(index, type);
    ptr->m_id = Id::UPDATE_TYPE;
    return Command(Id::UPDATE_TYPE, ptr);
}

Command Command::buildUpdateCommentCommand(int index, const QString& comment)
{
    QSharedPointer<UpdateCommand> ptr = QSharedPointer<UpdateCommand>::create(index, comment);
    ptr->m_id = Id::UPDATE_COMMENT;
    return Command(Id::UPDATE_COMMENT, ptr);
}
Command Command::deserialize(const QByteArray& bytes)
{
    static const int sizeofId = static_cast<int>(sizeof(Id));
    if (bytes.size() < sizeofId)
        return Command();

    QDataStream stream(bytes);
    quint8 idInt;
    stream >> idInt;
    Id id = static_cast<Id>(idInt);
    switch (id) {
    case Id::ADD:
    {
        QString name = Command::readUtf(stream);
        QString type = Command::readUtf(stream);
        QString comment = Command::readUtf(stream);
        return buildAppendCommand(name, type, comment);
    }
    case Id::REMOVE:
    {
        int index;
        stream >> index;
        return buildRemoveCommand(index);
    }
    case Id::UPDATE_NAME:
    case Id::UPDATE_TYPE:
    case Id::UPDATE_COMMENT:
    {
        int index;
        stream >> index;
        QString value = Command::readUtf(stream);
        if (id == Id::UPDATE_NAME)
            return buildUpdateNameCommand(index, value);
        if (id == Id::UPDATE_TYPE)
            return buildUpdateTypeCommand(index, value);
        return buildUpdateCommentCommand(index, value);
    }
    default:
        return Command();
    }
}

QByteArray Command::serialize() const
{
    if (m_command.isNull())
        return QByteArray();

    QByteArray bytes;
    QDataStream stream(&bytes, QIODevice::WriteOnly);
    stream << static_cast<quint8>(m_id);
    bytes.append(m_command->serialize());
    return bytes;
}

void Command::executeWith(AccountTable* accountTable) const
{
    executeWith(accountTable, nullptr);
}

void Command::executeWith(AccountTable* accountTable, AccountTableModel* accountTableModel) const
{
    if (!m_command.isNull())
        m_command->executeWith(accountTable, accountTableModel);
}

Command::Command(Id commandId, QSharedPointer<Command> command)
    : m_id(commandId)
    , m_command(command)
{
}

void Command::writeUtf(QDataStream& stream, const QString& param)
{
    QByteArray utf = param.toUtf8();
    int utfSize = utf.size();
    stream << utfSize;
    stream.writeRawData(utf.constData(), utfSize);
}

QString Command::readUtf(QDataStream& stream)
{
    int utfSize;
    stream >> utfSize;

    QByteArray data(utfSize, '\0');
    stream.readRawData(data.data(), utfSize);
    QString result = QString::fromUtf8(data.constData(), utfSize);
    return result;
}

AppendCommand::AppendCommand(const QString& name, const QString& type, const QString& comment)
    : m_name(name)
    , m_type(type)
    , m_comment(comment)
{
}

QByteArray AppendCommand::serialize() const
{
    QByteArray bytes;
    QDataStream stream(&bytes, QIODevice::WriteOnly);
    Command::writeUtf(stream, m_name);
    Command::writeUtf(stream, m_type);
    Command::writeUtf(stream, m_comment);

    return bytes;
}

void AppendCommand::executeWith(AccountTable* accountTable, AccountTableModel* accountTableModel) const
{
    if (accountTableModel) {
        accountTableModel->beginInsertOneRow(accountTable->size());
        accountTable->append(m_name, m_type, m_comment);
        accountTableModel->endInsertOneRow();
    } else {
        accountTable->append(m_name, m_type, m_comment);
    }
}

RemoveCommand::RemoveCommand(int index)
    : m_index(index)
{
}

QByteArray RemoveCommand::serialize() const
{
    QByteArray bytes;
    QDataStream stream(&bytes, QIODevice::WriteOnly);
    stream << m_index;

    return bytes;
}

void RemoveCommand::executeWith(AccountTable* accountTable, AccountTableModel* accountTableModel) const
{
    SelectableAccountTable* selectable = qobject_cast<SelectableAccountTable*>(accountTable);
    if (selectable) {
        if (selectable->rowIsSelected(m_index))
            selectable->resetSelectedRow();
        else if (selectable->selectedRow() > m_index)
            selectable->setSelectedRow(selectable->selectedRow() - 1);
    }

    if (accountTableModel) {
        accountTableModel->beginRemoveOneRow(m_index);
        accountTable->remove(m_index);
        accountTableModel->endRemoveOneRow();
    } else {
        accountTable->remove(m_index);
    }
}

UpdateCommand::UpdateCommand(int index, const QString& value)
    : m_index(index)
    , m_value(value)
{
}

QByteArray UpdateCommand::serialize() const
{
    QByteArray bytes;
    QDataStream stream(&bytes, QIODevice::WriteOnly);
    stream << m_index;
    Command::writeUtf(stream, m_value);

    return bytes;
}

void UpdateCommand::executeWith(AccountTable* accountTable, AccountTableModel* accountTableModel) const
{
    int column = 0;
    switch (m_id) {
    case Id::UPDATE_NAME:
        accountTable->updateName(m_index, m_value);
        column = 1;
        break;
    case Id::UPDATE_TYPE:
        accountTable->updateType(m_index, m_value);
        column = 2;
        break;
    case Id::UPDATE_COMMENT:
        accountTable->updateComment(m_index, m_value);
        column = 3;
        break;
    default:
        return;
    }

    if (accountTableModel) {
        SelectableAccountTable* selectable = qobject_cast<SelectableAccountTable*>(accountTable);
        if (selectable && !selectable->columnIsSelected(column))
            accountTableModel->updateCell(m_index, column);
    }
}
