#include "SharedDataService.h"
#include "Command.h"
#include "DiskStorageAccountTable.h"

SharedDataService::SharedDataService(const QString& serverName, const QString& storageFileName)
    : m_serverName(serverName)
    , m_fileName(storageFileName)
{
    m_accountTable.setAccounts(DiskStorageAccountTable::fromFile(storageFileName).accounts());
}

void SharedDataService::newConnection()
{
    while (m_server->hasPendingConnections()) {
        QLocalSocket* socket = m_server->nextPendingConnection();
        addNewSocket(socket);
    }
}

void SharedDataService::run()
{
    m_server = QSharedPointer<QLocalServer>::create();
    connect(m_server.data(), &QLocalServer::newConnection, this, &SharedDataService::newConnection, Qt::QueuedConnection);
    m_server->removeServer(m_serverName);
    m_server->listen(m_serverName);
}

void SharedDataService::socketDisconnected()
{
    QLocalSocket* socket = qobject_cast<QLocalSocket*>(sender());
    m_sender.removeSocket(socket);
    m_receivers.remove(socket);
    if (m_sender.countOfSockets() == 0) {
        SharedInstanceCounter instanceCounter;
        instanceCounter.lock();

        m_server->removeServer(m_serverName);
        DiskStorageAccountTable::toFile(m_fileName, m_accountTable);

        instanceCounter.decrement();
        instanceCounter.unlock();

        emit allConnectionsClosed();
    }
}

void SharedDataService::processCommand(const Command& command)
{
    command.executeWith(&m_accountTable);
}

void SharedDataService::addNewSocket(QLocalSocket* socket)
{
    connect(socket, &QLocalSocket::disconnected, this, &SharedDataService::socketDisconnected);
    connect(socket, &QLocalSocket::disconnected, socket, &QLocalSocket::deleteLater);
    m_sender.addSocket(socket);
    auto it = m_receivers.insert(socket, QSharedPointer<Receiver>::create());
    connect(socket, &QLocalSocket::readyRead, it.value().data(), &Receiver::readyRead);
    connect(it.value().data(), &Receiver::newCommand, this, &SharedDataService::processCommand);
    connect(it.value().data(), &Receiver::newCommand, this, &SharedDataService::sendCommandToAll);
    sendAccounts(socket);
}

void SharedDataService::sendCommandToAll(const Command& command)
{
    m_sender.sendCommandToAll(command);
}

void SharedDataService::sendAccounts(QLocalSocket* socket)
{
    for (int i = 0; i < m_accountTable.size(); ++i) {
        const Account& account = m_accountTable.account(i);
        Command command = Command::buildAppendCommand(account.name(), account.type(), account.comment());
        m_sender.sendCommandToSocket(socket, command);
    }
}
