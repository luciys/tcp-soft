#include "MainWindow.h"
#include "SharedDataService.h"
#include "SharedInstanceCounter.h"
#include <QApplication>
#include <QThread>

namespace {

const QString kServiceName = "kServiceName::LocalService";
const QString kFileNameStorage = "data.txt";

} // namespace

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    QSharedPointer<SharedDataService> service;
    QSharedPointer<QThread> thread;
    SharedInstanceCounter instanceCounter;
    instanceCounter.lock();
    if (instanceCounter.count() == 0) {
        QApplication::setQuitOnLastWindowClosed(false);
        service = QSharedPointer<SharedDataService>::create(kServiceName, kFileNameStorage);
        thread = QSharedPointer<QThread>::create();
        service->moveToThread(thread.data());
        QObject::connect(thread.data(), &QThread::started, service.data(), &SharedDataService::run);
        QObject::connect(service.data(), &SharedDataService::allConnectionsClosed, thread.data(), &QThread::quit);
        QObject::connect(service.data(), &SharedDataService::allConnectionsClosed, &a, &QApplication::quit);
        thread->start();
        instanceCounter.increment();
    }

    MainWindow mainWindow(kServiceName);
    mainWindow.show();

    instanceCounter.unlock();

    return a.exec();
}
