#ifndef PIXMAPPROVIDER_H
#define PIXMAPPROVIDER_H

#include <QPixmap>

class PixmapProvider
{
public:
    PixmapProvider() = delete;
    PixmapProvider(PixmapProvider&) = delete;
    ~PixmapProvider() = delete;

    static const QPixmap& appPixmap();
    static const QPixmap& phonePixmap();
    static const QPixmap& removePixmap();
    static const QPixmap& addPixmap();
};

#endif // PIXMAPPROVIDER_H
