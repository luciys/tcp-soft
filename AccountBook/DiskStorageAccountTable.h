#ifndef DISKSTORAGEACCOUNTTABLE_H
#define DISKSTORAGEACCOUNTTABLE_H

#include "AccountTable.h"
#include <QString>
#include <QDataStream>

class DiskStorageAccountTable
{
public:
    DiskStorageAccountTable() = delete;
    DiskStorageAccountTable(DiskStorageAccountTable&) = delete;
    ~DiskStorageAccountTable() = delete;

    static AccountTable fromFile(const QString& fileName);
    static void toFile(const QString& fileName, const AccountTable& accountTable);
};

#endif // DISKSTORAGEACCOUNTTABLE_H
