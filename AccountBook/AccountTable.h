#ifndef ACCOUNTTABLE_H
#define ACCOUNTTABLE_H

#include "Account.h"
#include <QList>
#include <QObject>

class AccountTable : public QObject
{
    Q_OBJECT
public:
    AccountTable();
    AccountTable(AccountTable& accountTable);

    bool append(const QString& name, const QString& type, const QString& comment);
    void remove(int row);

    void updateName(int row, const QString& name);
    void updateType(int row, const QString& type);
    void updateComment(int row, const QString& comment);

    int size() const;
    void setAccounts(const QList<Account>& accounts);
    const QList<Account>& accounts() const;
    const Account& account(int index) const;

    AccountTable& operator=(AccountTable& accountTable);

private:
    bool indexIsValid(int index) const;

private:
    QList<Account> m_accounts;
    int m_size;
};

#endif // ACCOUNTTABLE_H
