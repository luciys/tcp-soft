#ifndef SELECTABLEACCOUNTTABLE_H
#define SELECTABLEACCOUNTTABLE_H

#include "AccountTable.h"

class SelectableAccountTable : public AccountTable
{
    Q_OBJECT
public:
    SelectableAccountTable();

    void setSelectedRow(int row);
    int selectedRow() const;
    bool rowIsSelected(int row) const;
    void resetSelectedRow();

    void setSelectedColumn(int column);
    int selectedColumn() const;
    bool columnIsSelected(int column) const;
    void resetSelectedColumn();

private:
    int m_selectedRow;
    int m_selectedColumn;
};

#endif // SELECTABLEACCOUNTTABLE_H
