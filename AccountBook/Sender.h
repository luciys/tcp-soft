#ifndef SENDER_H
#define SENDER_H

#include "Command.h"
#include <QLocalSocket>

class Sender : public QObject
{
    Q_OBJECT
public:
    Sender();

    void addSocket(QLocalSocket* socket);
    void removeSocket(QLocalSocket* socket);
    int countOfSockets() const;
    void sendCommandToSocket(QLocalSocket* socket, const Command& command);

public Q_SLOTS:
    void sendCommandToAll(const Command& command);

private:
    void send(QLocalSocket* socket, const QByteArray& data);

private:
    QSet<QLocalSocket*> m_sockets;
};

#endif // SENDER_H
